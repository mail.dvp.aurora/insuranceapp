package com.example.insuranceapp.video_player


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import com.example.insuranceapp.R
import com.example.insuranceapp.dabi_prakriya.DabiPrakriyaActivity

import com.example.insuranceapp.databinding.ActivityVideoPlayerBinding
import com.example.insuranceapp.pasu_panchi_bima.PashuPanchiBimaActivity
import com.example.insuranceapp.utils.Constants
import com.example.insuranceapp.welcome_page.WelcomeScreen


class VideoPlayer : AppCompatActivity() {
    private var screen: String? = Constants.SECTION_1
    private lateinit var binding: ActivityVideoPlayerBinding
    private lateinit var videoView:VideoView
    private lateinit var navigationIntent:Intent



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        videoView = findViewById(R.id.videoView)
        retrieveIntentData()
        initListener()
    }

    private fun initListener() {
       videoView.setOnCompletionListener {
           navigate()
        }
    }

    private fun navigate() {
        startActivity(navigationIntent)
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
        finish()
    }

    private fun retrieveIntentData() {
        screen = intent.getStringExtra(Constants.SCREEN_TO_PLAY)
        startVideo()
    }


    private fun initBinding() {
        binding = ActivityVideoPlayerBinding.inflate(layoutInflater)
    }

    private fun startVideo() {
        val path = "android.resource://" + packageName + "/" + getScreen()
        videoView.setVideoURI(Uri.parse(path))
        videoView.start()
    }

    private fun getScreen(): Int {
        when (screen) {
            Constants.SECTION_2 -> {
                navigationIntent = Intent(this, DabiPrakriyaActivity::class.java)
                return R.raw.section_2
            }

            Constants.SECTION_3 -> {
                navigationIntent = Intent(this, WelcomeScreen::class.java)
                return R.raw.section_3
            }
            Constants.MAIN_SHUFFLED_VIDEO->{
                navigationIntent = Intent(this, WelcomeScreen::class.java)
                return R.raw.main_section_shuffled
            }
            else-> {
                navigationIntent = Intent(this, WelcomeScreen::class.java)
                return R.raw.main_comp
            }
        }

    }
}