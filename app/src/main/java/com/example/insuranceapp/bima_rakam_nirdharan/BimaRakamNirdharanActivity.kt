package com.example.insuranceapp.bima_rakam_nirdharan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.insuranceapp.R
import com.example.insuranceapp.dabi_prakriya.DabiPrakriyaActivity
import com.example.insuranceapp.databinding.ActivityBimaRakamNirdharanBinding
import com.example.insuranceapp.utils.Constants
import com.example.insuranceapp.video_player.VideoPlayer

class BimaRakamNirdharanActivity : AppCompatActivity() {

    private lateinit var binding:ActivityBimaRakamNirdharanBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        initListener()
    }

    private fun initListener() {
        binding.btnNext.setOnClickListener {
            var navigationIntent = Intent(this,VideoPlayer::class.java).apply {
                putExtra(Constants.SCREEN_TO_PLAY,Constants.SECTION_2)
            }
            startActivity(navigationIntent)
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
            finish()
        }
    }

    private fun initBinding() {
        binding = ActivityBimaRakamNirdharanBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.tvTitle.text = resources.getString(R.string.bima_rakam_nirdharan)

    }
}