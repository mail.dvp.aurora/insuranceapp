package com.example.insuranceapp.afno_bebashaya

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.insuranceapp.R
import com.example.insuranceapp.databinding.ActivityAafnoBebashayaBinding
import com.example.insuranceapp.pasu_panchi_bima.PashuPanchiBimaActivity
import com.google.android.material.snackbar.Snackbar

class AafnoBebashaya : AppCompatActivity() {

    private lateinit var llPasuPanchiBima : LinearLayout
    private lateinit var llBaliBima : LinearLayout
    private lateinit var viewOutlineTopLeft : View
    private lateinit var viewOutlineTopRight : View
    private lateinit var coordinatorLayout : CoordinatorLayout
    private lateinit var buttonNext : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aafno_bebashaya)
        initViews()
        initListener()

    }

    private fun initViews() {
        llPasuPanchiBima = findViewById(R.id.ll_pasu_panchi_bima)
        llBaliBima = findViewById(R.id.ll_bali_bima)
        viewOutlineTopLeft = findViewById(R.id.view_outline_top_left)
        viewOutlineTopRight = findViewById(R.id.view_outline_top_right)
        coordinatorLayout = findViewById(R.id.coordinatorLayout)
        buttonNext = findViewById(R.id.btnNext)
    }

    private fun initListener() {
        buttonNext.setOnClickListener {
            startActivity(Intent(this,PashuPanchiBimaActivity::class.java))
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
            finish()
        }

       llPasuPanchiBima.setOnClickListener {
           viewOutlineTopLeft.visibility = View.GONE
            viewOutlineTopRight.visibility  = View.VISIBLE
        }
       llBaliBima.setOnClickListener {
           Snackbar.make(coordinatorLayout, "Feature Coming Soon!!!", Snackbar.LENGTH_LONG).show()
        }
    }


}