package com.example.insuranceapp.home_page

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.insuranceapp.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}