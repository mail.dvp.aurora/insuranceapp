package com.example.insuranceapp.dabi_prakriya

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.insuranceapp.R
import com.example.insuranceapp.afno_bebashaya.AafnoBebashaya
import com.example.insuranceapp.databinding.ActivityDabiPrakriyaBinding
import com.example.insuranceapp.utils.Constants
import com.example.insuranceapp.video_player.VideoPlayer

class DabiPrakriyaActivity : AppCompatActivity() {
    private lateinit var binding :  ActivityDabiPrakriyaBinding
    private var selectionCount:Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        initListener()
    }

    private fun initListener() {
        binding.rb1.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }
        binding.rb2.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }
        binding.rb3.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }
        binding.rb4.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }

        binding.btnNext.setOnClickListener {
            var navigationIntent = Intent(this,VideoPlayer::class.java).apply {
                putExtra(Constants.SCREEN_TO_PLAY,Constants.SECTION_3)
            }
            startActivity(navigationIntent)
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
            finish()
        }
    }

    private fun checkCountAndEnableButton() {
        if(selectionCount>=4){
            binding.btnNext.isEnabled = true
            binding.btnNext.alpha = 1f
        }
    }

    private fun initBinding() {
        binding = ActivityDabiPrakriyaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.tvTitle.text = resources.getString(R.string.dabi_prakriya)
    }
}