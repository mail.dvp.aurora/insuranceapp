package com.example.insuranceapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.insuranceapp.utils.Constants
import com.example.insuranceapp.video_player.VideoPlayer

class SplashScreen : AppCompatActivity() {
    private  val navigationIntent:Intent by lazy {
        Intent(this,VideoPlayer::class.java).apply {
            putExtra(Constants.SCREEN_TO_PLAY,Constants.MAIN_SHUFFLED_VIDEO)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(navigationIntent)
            finish()
        }, 1500)
    }
}