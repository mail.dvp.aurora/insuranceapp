package com.example.insuranceapp.pasu_panchi_bima

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.insuranceapp.R
import com.example.insuranceapp.bima_coverage.BimaCoverage
import com.example.insuranceapp.bima_rakam_nirdharan.BimaRakamNirdharanActivity
import com.example.insuranceapp.databinding.ActivityPashuPanchiBimaBinding

class PashuPanchiBimaActivity : AppCompatActivity() {
    private lateinit var binding:ActivityPashuPanchiBimaBinding
    private var selectionCount:Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        initListener()
    }

    private fun initListener() {
        binding.rb1.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }
        binding.rb2.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }
        binding.rb3.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }
        binding.rb4.setOnClickListener{
            selectionCount+=1
            checkCountAndEnableButton()
        }

        binding.btnNext.setOnClickListener {
            startActivity(Intent(this,BimaCoverage::class.java))
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
            finish()
        }
    }

    private fun checkCountAndEnableButton() {
        if(selectionCount>=4){
            binding.btnNext.alpha = 1f
            binding.btnNext.isEnabled = true
        }
    }

    private fun initBinding() {
        binding = ActivityPashuPanchiBimaBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}