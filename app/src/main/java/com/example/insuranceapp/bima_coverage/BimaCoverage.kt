package com.example.insuranceapp.bima_coverage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.insuranceapp.R
import com.example.insuranceapp.bima_rakam_nirdharan.BimaRakamNirdharanActivity
import com.example.insuranceapp.databinding.ActivityBimaCoverageBinding

class BimaCoverage : AppCompatActivity() {
    private lateinit var binding:ActivityBimaCoverageBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        initListener()
    }

    private fun initListener() {
        binding.btnNext.setOnClickListener {
            startActivity(Intent(this,BimaRakamNirdharanActivity::class.java))
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left)
            finish()
        }
    }

    private fun initBinding() {
        binding = ActivityBimaCoverageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.tvTitle.text = resources.getString(R.string.bima_coverage)
    }
}