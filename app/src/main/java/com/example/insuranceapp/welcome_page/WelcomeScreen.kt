package com.example.insuranceapp.welcome_page

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.insuranceapp.R
import com.example.insuranceapp.afno_bebashaya.AafnoBebashaya
import com.example.insuranceapp.databinding.ActivityWelcomeScreenBinding
import com.example.insuranceapp.utils.Constants
import com.example.insuranceapp.video_player.VideoPlayer

class WelcomeScreen : AppCompatActivity() {
    private lateinit var navigationIntent: Intent
    private  val btnProceed : Button by lazy {
        findViewById(R.id.proceed)
    }

    private  val btnCancel : Button by lazy {
        findViewById(R.id.cancel)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_screen)
        initListener()
    }

    private fun initListener() {
        btnProceed.setOnClickListener {
            navigationIntent = Intent(this,AafnoBebashaya::class.java)
            startActivity(navigationIntent)
            finish()
        }

        btnCancel.setOnClickListener {
            navigationIntent = Intent(this,VideoPlayer::class.java).apply {
                putExtra(Constants.SCREEN_TO_PLAY,Constants.MAIN_COMPOSITION)
            }
            startActivity(navigationIntent)
            finish()
        }
    }


}