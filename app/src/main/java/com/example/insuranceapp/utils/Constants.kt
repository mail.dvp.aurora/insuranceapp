package com.example.insuranceapp.utils

object Constants {
     const val SCREEN_TO_PLAY ="screen_to_play"
     const val SECTION_1 = "section_1"
     const val SECTION_2 = "section_2"
     const val SECTION_3 = "section_3"
     const val MAIN_SHUFFLED_VIDEO = "main_shuffled_video"
     const val MAIN_COMPOSITION = "main_composition"
}